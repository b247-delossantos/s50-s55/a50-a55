import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/ErrorPage'
import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


function App() {

  // State hook for the user state that's defined here is for a global scope
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  //const [user, setUser] = useState({ email : localStorage.getItem("email") })

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem(`token`)}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id:  data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  // Common pattern in react.js for a component to return multiple elements.
  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses/>} />
            <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            {/*
              "*" - is a wildcard character that will match any path that has not already been matched by previous routes.
            */}
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
